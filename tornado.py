flag = True

while flag:
    n = int(input())

    if n == 0:
        flag = False
    else:
        posts = input().split()

        posts_count = 0
        consecutive_broken_posts = 0

        for i in range(n):
            post = posts[i]

            if post == "1":
                consecutive_broken_posts = 0
            else:
                consecutive_broken_posts += 1

                if consecutive_broken_posts > 0 and consecutive_broken_posts % 2 == 0:
                    posts_count += 1

        print(posts_count)
